from flask import request
from base64 import b64encode,b64decode,decodebytes,decode
import requests
import pika
import json
import redis
import settings

# Config Redis
redis_host = settings.REDIS_HOST
redis_port = settings.REDIS_POST
redis_password = settings.REDIS_PASSWORD

# Connect Rabbitwq with pika
connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='handel_cache', durable=True)
print(' [*] Waiting for messages. To exit press CTRL+C')

def callback(ch, method, properties, body):
    # Connect Redis
    r = redis.StrictRedis(host=redis_host, port=redis_port, password=redis_password, decode_responses=True)
    # Get data from Redis
    # convert data json into dict
    data_json = json.loads(body)
    url = data_json['url']

    if 'expire' in data_json:

        expire = data_json['expire']
        data = requests.get(url).text
        # Encode data
        data_byte = data.encode('utf-8')
        encode_data = b64encode(data_byte)
        # Save cache in Redis
        r.set("msg:" + url, encode_data, ex=expire)
        print('save cache ')
    else:
        # Delete cache from Redis
        r.delete('msg:'+url)
        print('delete cache')
    ch.basic_ack(delivery_tag=method.delivery_tag)

channel.basic_qos(prefetch_count=1)

channel.basic_consume(queue='handel_cache', on_message_callback=callback)

channel.start_consuming()