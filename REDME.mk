BT 2. Dựng Webservices / API set cache / Xóa cache
 2.1  Dựng 1 web Flask có param <page> dynamic làm Backend
kiểu @app.route("/<page>", methods = ["GET"])
def change_page(page):

Nó sẽ lấy trên 1 site nhất định
VD : file settings set domain : soha.vn
nó sẽ lấy nội dung : soha.vn/<page>
Lưu nội dung vào redis trong 20 phút
key là uri : http://soha.vn/kinh-doanh.htm
content có thể mã hóa 2 chiều để lưu , giảm dung lượng lưu trữ
Khi get ra thì nếu có page trong redis thì lấy từ redis ra
nếu ko có trong redis thì chui vào domain/<page> lấy ra

2.2 : Dựng API :
 -  Gọi setcache :
    End point : /handel_cache/<domain>
    POST {'url':'/xxx', 'expire': xxx}
    --> nó sẽ chui vào link : http://domain/url để lấy content , set vào redis làm cache với thời gian expire
- Xóa cache
   End point : /handel_cache/<domain>
   DELETE  { 'url':'/xxx' }
    --> Xóa cache trong Redis link http://domain/url

Yêu cầu : Có authen qua header Authorization . Sử dụng celery hoặc hotqueue hoặc Rabbit để xử lí các message job của API
Mô hình VD : Request setcache / xóa cache --> API --> Push message to Rabbit MQ --> bật 4 worker consumer get message rồi xử lí .

2.3 Dựng 1 con proxy = nginx port 80 , HA Backend web qua 2 port 8088 và 8089
     nginx port 90 , HA cụm API set cache / xóa cache qua 4 port 8100, 8200,8300,8400