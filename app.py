from flask import Flask,render_template, request
from flask_restful import Resource, Api,reqparse
import requests
from functools import wraps
from base64 import b64encode,b64decode,decodebytes,decode
import redis
import settings
import pika
import json



app = Flask(__name__)
api = Api(app)

parser = reqparse.RequestParser()

app.config['SECRET_KEY'] = settings.SECRET_KEY
# Config Redis
redis_host = settings.REDIS_HOST
redis_port = settings.REDIS_POST
redis_password = settings.REDIS_PASSWORD

@app.route('/')
def index():
    return render_template('home.html')
@app.route("/page1")
def page1():
    return render_template('page1.html')

@app.route('/xc/<page>', methods = ["GET"])
def change_page(page):
    # Connect Redis
    r = redis.StrictRedis(host = redis_host,
                          port = redis_port,
                          password=redis_password,
                          decode_responses=True)
    # check cache exists
    if r.exists('msg:'+page):
        print("existed!")
        # get data cache from redis
        d=r.get("msg:"+page)
        # decode data and return
        d_b=d.encode('utf-8')
        decode_d = decodebytes(d_b)
        da = decode_d.decode()
        return da
    # cache not exists
    # get data from url
    data = requests.get('http://'+settings.HOST+":"+str(settings.PORT)+"/"+page).text
    # encode data
    data_byte = data.encode('utf-8')
    encode_data = b64encode(data_byte)
    print('not existed!')
    # seve data in redis
    save_redis = r.set("msg:"+page, encode_data, ex = 2*60)
    return data


# check auth
def auth(f):
    @wraps(f)
    def check(*args, **kwargs):
        # check token exists
        if 'token' in request.headers:
            # check token correct
            if request.headers['token'] == settings.SECRET_KEY:
                return f(*args, **kwargs)
            return {"msg":"Auth failed "}
    return check
# class api post and delete cache
class handel_cache(Resource):

    @auth
    # api post cache
    def post(self):
        # get data from request
        data_json = request.get_json()
        # connect rabbitmq with pika
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(host='localhost'))
        channel = connection.channel()

        channel.queue_declare(queue='handel_cache', durable=True)
        # convert data into json
        message = json.dumps(data_json)
        print(data_json)
        # sent data to rabbitmq with queue ='handel_cache'
        channel.basic_publish(
            exchange='',
            routing_key='handel_cache',
            body=message,
            properties=pika.BasicProperties(
                delivery_mode=2,
            ))
        connection.close()
        return 'save cache'

    @auth
    # api delete cache
    def delete(self):

        # get data from request
        data_json = request.get_json()
        # connect rabbitmq with pika
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(host='localhost'))
        channel = connection.channel()

        channel.queue_declare(queue='handel_cache', durable=True)
        # convert data into json
        message = json.dumps(data_json)
        print(data_json)
        # sent data to rabbitmq with queue ='handel_cache'
        channel.basic_publish(
            exchange='',
            routing_key='handel_cache',
            body=message,
            properties=pika.BasicProperties(
                delivery_mode=2,  # make message persistent
            ))
        connection.close()
        return 'delete cache'

api.add_resource(handel_cache,'/handel_cache/')
if __name__ == '__main__':
    app.run(host =settings.HOST,
            port = settings.PORT,
            debug=True)